package com.wangl.spring.suggestion.filetype;

import com.intellij.openapi.fileTypes.LanguageFileType;
import com.wangl.spring.utils.Icons;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.yaml.YAMLLanguage;

import javax.swing.*;

/**
 * @ClassName SpringConfigurationYamlFileType
 * @Description TODO
 * @Author wangl
 * @Date 2023/4/20 10:53
 */
public class SpringConfigurationYamlFileType extends LanguageFileType {

    public static final SpringConfigurationYamlFileType INSTANCE = new SpringConfigurationYamlFileType();

    private SpringConfigurationYamlFileType() {
        super(YAMLLanguage.INSTANCE, true);
    }

    @Override
    public @NonNls @NotNull String getName() {
        return "spring-boot-configuration-yaml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Spring yaml configuration file";
    }

    @Override
    public @NotNull String getDefaultExtension() {
        return "yaml";
    }

    @Override
    public Icon getIcon() {
        return Icons.spring_configuration;
    }
}
