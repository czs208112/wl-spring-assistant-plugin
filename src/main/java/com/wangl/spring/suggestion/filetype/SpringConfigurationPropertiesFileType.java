package com.wangl.spring.suggestion.filetype;

import com.intellij.lang.properties.PropertiesLanguage;
import com.intellij.openapi.fileTypes.LanguageFileType;
import com.wangl.spring.utils.Icons;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;

/**
 * @ClassName SpringConfigurationPropertiesFileType
 * @Description TODO
 * @Author wangl
 * @Date 2023/4/20 10:53
 */
public class SpringConfigurationPropertiesFileType extends LanguageFileType {

    public static final SpringConfigurationPropertiesFileType INSTANCE = new SpringConfigurationPropertiesFileType();

    private SpringConfigurationPropertiesFileType() {
        super(PropertiesLanguage.INSTANCE, true);
    }

    @Override
    public @NonNls @NotNull String getName() {
        return "spring-boot-configuration-properties";
    }

    @Override
    public @NotNull String getDescription() {
        return "Spring properties configuration file";
    }

    @Override
    public @NotNull String getDefaultExtension() {
        return "properties";
    }

    @Override
    public Icon getIcon() {
        return Icons.spring_configuration;
    }
}
