package com.wangl.spring.utils;

import org.springframework.util.StringUtils;

import java.util.HashSet;

/**
 * @ClassName PlaceholderResolver
 * @Description TODO
 * @Author wangl
 * @Date 2023/4/20 16:32
 */
public class PlaceholderResolver {

    public static final String placeholderPrefix = "${";
    public static final String placeholderSuffix = "}";
    public static final String valueSeparator = ":";

    public static String getPlaceholder(String value){
        if (value.startsWith(placeholderPrefix) && value.endsWith(placeholderSuffix)){
            String placeholder = value.substring(placeholderPrefix.length(), value.length() - 1);
            if (placeholder.contains(valueSeparator)){
                return placeholder.substring(0, placeholder.indexOf(valueSeparator));
            }
            return placeholder;
        }
        return "";
    }
}
